mvn io.quarkus:quarkus-maven-plugin:1.11.7.Final:create \
    -DprojectGroupId=com.example \
    -DprojectArtifactId=catalog \
    -DclassName="com.example.CatalogResource" \
    -Dpath="/getCatalog"

 ./mvnw quarkus:add-extension -Dextensions="openshift"

 ./mvnw clean package -Dquarkus.kubernetes.deploy=true