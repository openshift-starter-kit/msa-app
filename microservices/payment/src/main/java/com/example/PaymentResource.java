package com.example;

import org.jboss.logging.Logger;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/payment")
public class PaymentResource {

    private static final Logger LOG = Logger.getLogger(PaymentResource.class);

    @Inject
    PaymentService service;

    @GET
    public String payment(@QueryParam("total") String total) {
        LOG.info("received: " + total);
        return service.payment(total);
    }
}