package com.example;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PaymentService {

    public String payment(String total) {
        return "Payment success for the amount  " + total;
    }

}
